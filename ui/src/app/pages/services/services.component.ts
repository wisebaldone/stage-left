import { Component } from '@angular/core';
import {AppState} from "../../state/app.state";
import {Store} from "@ngrx/store";

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent {

  constructor(
    private store: Store<AppState>,
  ) { }

  ngOnInit(): void {
  }

}
