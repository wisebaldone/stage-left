import {Component, effect, ViewChild} from '@angular/core';
import {Alignments, ChartTheme, GaugeChart, GaugeChartOptions, GaugeTypes, Statuses} from "@carbon/charts";
import {TableItem, TableModel} from "carbon-components-angular";
import {Store} from "@ngrx/store";
import {selectTheme} from "../../state/theme/theme.selector";



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  protected unassignedAssetsOptions: GaugeChartOptions = {
    title: 'Unassigned Assets %',
    resizable: true,
    height: '250px',
    gauge: { status: Statuses.WARNING, type: GaugeTypes.FULL, alignment: Alignments.CENTER },
    color: { scale: { value: '#b23d4f' } },
    toolbar: { enabled: false },
  }
  protected unassignedAssetsData = [{group: 'value', value: 10}];
  protected upcomingChangesModel: TableModel = new TableModel();

  @ViewChild('unassignedChart') unassignedChart: GaugeChart | undefined;

  // @ts-ignore
  protected theme = this.store.selectSignal(selectTheme);

  constructor(private store: Store) {

    effect(() => {
      this.unassignedAssetsOptions = {...this.unassignedAssetsOptions, theme: this.theme().themeCarbon as ChartTheme};
      this.unassignedChart?.model.setOptions(this.unassignedAssetsOptions);
    })

    this.upcomingChangesModel.data = [
      [new TableItem({data: "example entry"})]
    ];
  }
}
