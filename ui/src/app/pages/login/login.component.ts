import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { IconService} from 'carbon-components-angular/icon';

import {
  ArrowRight16
} from '@carbon/icons';
import { AuthService } from '../../features/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  loginForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl('')
  });

  constructor(protected iconService : IconService, protected authService : AuthService, private router: Router) {
  }

  ngOnInit() {
    this.iconService.registerAll([
      ArrowRight16
    ]);

    this.authService.checkLogin().subscribe((res:boolean) => {
      if (res) {
        this.router.navigate(['/dashboard']);
      }
    });
  }

  login() {
    let payload = this.loginForm.value;
    if (!payload['username'] || !payload['password']) {
      console.error('Username and password are required');
      return;
    }
    this.authService.login(payload['username'], payload['password']).subscribe((res:boolean) => {
      if (res) {
        this.router.navigate(['/dashboard']);
      } else {
        console.log('Login failed');
      }
    });
  }
}
