import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { GridModule } from 'carbon-components-angular/grid';
import { UIShellModule } from 'carbon-components-angular/ui-shell';
import { IconModule } from 'carbon-components-angular/icon';
import { DropdownModule } from 'carbon-components-angular/dropdown';
import { BreadcrumbModule } from 'carbon-components-angular/breadcrumb';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthService } from './features/services/auth.service';
import { AuthGuard } from './features/guards/auth.guard';
import { LoginComponent } from './pages/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { HeaderComponent } from './shared/header/header.component';
import {
  LinkModule,
  ThemeModule,
  InputModule,
  ButtonModule,
  ToggleModule,
  PopoverModule,
  LayerModule,
  TilesModule,
  TableModule,
  PaginationModule,
  DialogModule,
  CheckboxModule,
  NotificationModule, LoadingModule
} from "carbon-components-angular";
import {ChartsModule} from "@carbon/charts-angular";
import { ServicesComponent } from './pages/services/services.component';
import { StoreModule } from '@ngrx/store';
import { themeReducer } from "./state/theme/theme.reducer";
import { EffectsModule } from '@ngrx/effects';
import {ThemeEffects} from "./state/theme/theme.effects";
import { ServicePagedListComponent } from './shared/service-paged-list/service-paged-list.component';
import { DisabledComponent } from './pages/disabled/disabled.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PageNotFoundComponent,
    DashboardComponent,
    LoginComponent,
    ServicesComponent,
    ServicePagedListComponent,
    DisabledComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    GridModule,
    BreadcrumbModule,
    UIShellModule,
    IconModule,
    ButtonModule,
    InputModule,
    DropdownModule,
    LinkModule,
    ThemeModule,
    ToggleModule,
    FormsModule,
    PopoverModule,
    LayerModule,
    TilesModule,
    ChartsModule,
    TableModule,
    StoreModule.forRoot({
      theme: themeReducer,
    }, {}),
    EffectsModule.forRoot([
      ThemeEffects
    ]),
    PaginationModule,
    DialogModule,
    CheckboxModule,
    NotificationModule,
    LoadingModule
  ],
  providers: [AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
