import {setTheme, toggleTheme} from "./theme.actions";
import {createReducer, on} from "@ngrx/store";
import {ThemeType} from "carbon-components-angular";

export interface ThemeState {
  theme: string;
  themeCarbon: ThemeType;
}

export const initialState: ThemeState = {
  theme: 'light',
  themeCarbon: 'white'
};

export const themeReducer = createReducer(
  initialState,
  on(setTheme, (state, { theme }) => ({
    ...state,
    theme: theme,
    themeCarbon: theme == 'light' ? 'white' : 'g100'
  })),
  on(toggleTheme, (state) => ({
    ...state,
    theme: state.theme === 'light' ? 'dark' : 'light',
    themeCarbon: state.themeCarbon == 'white' ? 'g100' : 'white'
  }))
);
