import {Injectable} from "@angular/core";
import {AppState} from "../app.state";
import {Store} from "@ngrx/store";
import {ThemeService} from "../../features/services/theme.service";
import {Actions, concatLatestFrom, createEffect, ofType} from "@ngrx/effects";
import {loadTheme, setTheme, toggleTheme} from "./theme.actions";
import {exhaustMap, from, of, switchMap, withLatestFrom} from "rxjs";
import {catchError, map, tap} from "rxjs/operators";
import {selectTheme} from "./theme.selector";

@Injectable()
export class ThemeEffects {

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private themeService: ThemeService
  ) {
  }

  loadTheme$ = createEffect(() => this.actions$.pipe(
    ofType(loadTheme),
    exhaustMap(() =>
      from([this.themeService.getTheme()]).pipe(
        map(theme => setTheme({theme}))
      )
    ),
  ));

  saveTheme$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(setTheme, toggleTheme),
        withLatestFrom(this.store.select(selectTheme)),
        tap(([action, theme]) => {
          this.themeService.setTheme(theme.theme);
        })
      ),
    {dispatch: false}
  );
}
