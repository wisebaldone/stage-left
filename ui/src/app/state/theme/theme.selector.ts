import {AppState} from "../app.state";


export const selectTheme = (state: AppState) => state.theme;
