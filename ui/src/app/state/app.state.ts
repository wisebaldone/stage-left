import {ThemeState} from "./theme/theme.reducer";


export interface AppState {
  theme: ThemeState;
}
