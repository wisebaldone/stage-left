import {AfterViewInit, Component, computed, effect, signal, TemplateRef, ViewChild} from '@angular/core';
import {ServicePagedListStore} from "./service-paged-list.store";
import {TableHeaderItem, TableItem, TableModel} from "carbon-components-angular";

@Component({
  selector: 'app-service-paged-list',
  templateUrl: './service-paged-list.component.html',
  styleUrls: ['./service-paged-list.component.scss'],
  providers: [ServicePagedListStore]
})
export class ServicePagedListComponent {

  model = new TableModel();
  services = this.localState.services;
  servicesAsTableItem = computed(() => {
    let items = new Array<TableItem[]>();
    this.services().forEach(service => {
      items.push([
        new TableItem({data: service.name}),
        new TableItem({data: service.identifier}),
        new TableItem({data: service.classification, template: this.colTypeTemplate}),
        new TableItem({data: {stage: service.life_cycle_stage, status: service.life_cycle_status}, template: this.colLifeCycleTemplate}),
      ]);
    });
    return items;
  });
  status = this.localState.status;
  page = this.localState.page;
  total = this.localState.total;

  showActive = true;
  showArchived = false;
  pageLengths = [25, 50, 100];

  @ViewChild('colTypeTemplate')
  protected colTypeTemplate: TemplateRef<any> | undefined;

  @ViewChild('colLifeCycleTemplate')
  protected colLifeCycleTemplate: TemplateRef<any> | undefined;

  constructor(private readonly localState: ServicePagedListStore) {
    this.model.pageLength = this.pageLengths[0];
    this.model.header = [
      new TableHeaderItem({data: 'Name'}),
      new TableHeaderItem({data: 'ID'}),
      new TableHeaderItem({data: 'Type'}),
      new TableHeaderItem({data: 'Status'}),
    ]
    effect(() => {
      this.model.totalDataLength = this.total();
      this.model.data = this.servicesAsTableItem();
      this.model.currentPage = this.page();
    })
  }

  ngOnInit(): void {
    this.selectPage();
  }

  selectPage(page: number = 1) {
    this.localState.loadPage({page: page, pageSize: this.model.pageLength});
    this.localState.getServices();
  }

  setShowActive(showActive: boolean) {
    this.showActive = showActive;
    this.selectPage();
  }

  setShowArchived(showArchived: boolean) {
    this.showArchived = showArchived;
    this.selectPage();
  }
}
