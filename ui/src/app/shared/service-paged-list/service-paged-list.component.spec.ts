import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicePagedListComponent } from './service-paged-list.component';

describe('ServicePagedListComponent', () => {
  let component: ServicePagedListComponent;
  let fixture: ComponentFixture<ServicePagedListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ServicePagedListComponent]
    });
    fixture = TestBed.createComponent(ServicePagedListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
