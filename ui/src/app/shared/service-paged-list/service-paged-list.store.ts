import {Service} from "../../features/models/cmdb/service.model";
import {Injectable} from "@angular/core";
import {ComponentStore, tapResponse} from "@ngrx/component-store";
import {Observable, switchMap, withLatestFrom} from "rxjs";
import {ServiceService} from "../../features/services/cmdb/service.service";
import {HttpErrorResponse} from "@angular/common/http";

export interface ServicePagedListState {
  page: number;
  pageSize: number;
  filter: string;
  total: number;
  services: Service[];
  error: null|string;
  status: 'pending' | 'loading' | 'success' | 'error';
}


@Injectable()
export class ServicePagedListStore extends ComponentStore<ServicePagedListState> {
  constructor(private serviceService: ServiceService) {
    super({
      page: 1,
      pageSize: 25,
      filter: '',
      total: 0,
      services: [],
      error: null,
      status: 'pending'
    });
  }

  readonly services = this.selectSignal((s) => s.services);
  readonly page = this.selectSignal((s) => s.page);
  readonly pageSize = this.selectSignal((s) => s.pageSize);
  readonly filter = this.selectSignal((s) => s.filter);
  readonly total = this.selectSignal((s) => s.total);
  readonly error = this.selectSignal((s) => s.error);
  readonly status = this.selectSignal((s) => s.status);

  // Actions
  readonly loadPage = this.updater((state, {page, pageSize}: {page: number, pageSize: number}) => ({
    ...state,
    page,
    pageSize,
    status: 'loading'
  }));

  readonly loadPageSuccessful = this.updater((state, {services, total, page}: {services: Service[], total: number, page: number}) => ({
    ...state,
    page,
    services,
    total,
    status: 'success'
  }));

  readonly loadPageFailed = this.updater((state, error: string) => ({
    ...state,
    error,
    status: 'error'
  }));


  // Effects
  readonly getServices = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(this.select((s) => s)),
      switchMap(([$action, state]) => this.serviceService.getServices(state.pageSize, state.page).pipe(
        tapResponse(
          (services) => this.loadPageSuccessful({services: services.data, total: services.meta.total, page: services.meta.current_page}),
          (error: HttpErrorResponse) => {console.log(error.message); this.loadPageFailed('Error loading services')}
        )
      ))
    )}
  );
}
