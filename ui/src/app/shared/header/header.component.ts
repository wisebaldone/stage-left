import { Component } from '@angular/core';
import { IconService} from 'carbon-components-angular/icon';

import {
  Switcher20, Search20,
  Help20, User32, Notification20,
  User20, ContainerSoftware16,
  Sprout16, Devices16,
  InProgress16, Checkmark16,
  Dashboard16, Chip16, Filter16, WarningHex16, InfrastructureClassic16,
  Industry16, Application16, Pending16, Undefined16, CheckmarkOutline16
} from '@carbon/icons';
import { NavigationItem } from 'carbon-components-angular/ui-shell';
import { AuthService } from '../../features/services/auth.service';
import { User } from '../../features/services/user';
import {AppState} from "../../state/app.state";
import {Store} from "@ngrx/store";
import {selectTheme} from "../../state/theme/theme.selector";
import {toggleTheme} from "../../state/theme/theme.actions";



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {

  protected showInternalRoutes = false;
  protected active = false;
  protected headerItems: NavigationItem[] = [
    {
      type: "menu",
      title: "Documentation",
      menuItems: [
        {
          content: "User Guide",
          href: "#"
        },
        {
          content: "API Reference",
          href: "#"
        },
      ],
    },
  ];

  protected loggedInSubscription: any;
  protected user: User|null = null;

  // panels
  protected profilePanelExpanded = false;
  protected notificationPanelExpanded = false;
  protected helpPanelExpanded = false;

  protected theme = this.store.selectSignal(selectTheme);

  constructor(
    private store: Store<AppState>,
    protected iconService : IconService,
    private authService : AuthService) {

    this.loggedInSubscription = this.authService.userChanged$.subscribe((user) => {
      this.showInternalRoutes = user !== null;
      this.user = user;
      if (user === null) {
        this.resetHeader();
      }
    });

    // preload any cached user
    this.showInternalRoutes = this.authService.isLoggedIn();
    this.user = this.authService.getUser();

  }

  ngOnInit() {
    this.iconService.registerAll([
      Switcher20,
      Search20,
      Help20,
      Notification20,
      User20,
      User32,
      ContainerSoftware16,
      Sprout16,
      Devices16,
      InProgress16,
      Checkmark16,
      Dashboard16,
      Chip16,
      Filter16,
      WarningHex16,
      InfrastructureClassic16,
      Industry16,
      Application16,
      Pending16,
      Undefined16,
      CheckmarkOutline16
    ]);
  }


  ngOnDestroy() {
    this.loggedInSubscription.unsubscribe();
  }

  protected logout() {
    this.authService.logout().subscribe({});
  }

  protected resetHeader() {
    this.profilePanelExpanded = false;
    this.notificationPanelExpanded = false;
    this.helpPanelExpanded = false;
  }
  protected toggleTheme() {
    this.store.dispatch(toggleTheme());
  }
}
