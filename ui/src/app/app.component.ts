import { Component } from '@angular/core';
import {AppState} from "./state/app.state";
import {Store} from "@ngrx/store";
import {selectTheme} from "./state/theme/theme.selector";
import {loadTheme} from "./state/theme/theme.actions";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  protected title: string = 'Stage Left';
  protected theme = this.store.selectSignal(selectTheme);

  constructor(private store : Store<AppState>) {
  }

  ngOnInit() {
    this.store.dispatch(loadTheme());
  }
}
