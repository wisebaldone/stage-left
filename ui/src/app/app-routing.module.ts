import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './features/guards/auth.guard';
import { LoginComponent } from './pages/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { ServicesComponent } from "./pages/services/services.component";
import {DisabledComponent} from "./pages/disabled/disabled.component";

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'cmdb/services', component: ServicesComponent, canActivate: [AuthGuard] },
  { path: 'cmdb/changes', component: DisabledComponent, canActivate: [AuthGuard] },
  { path: 'cmdb/assets', component: DisabledComponent, canActivate: [AuthGuard] },
  { path: 'incidents', component: DisabledComponent, canActivate: [AuthGuard] },
  { path: 'campaigns', component: DisabledComponent, canActivate: [AuthGuard] },
  { path: 'scorecards', component: DisabledComponent, canActivate: [AuthGuard] },
  { path: 'settings', component: DisabledComponent, canActivate: [AuthGuard] },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
