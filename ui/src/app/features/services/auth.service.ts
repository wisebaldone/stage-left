import { EventEmitter, Injectable, Output } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, Subject, throwError } from 'rxjs';
import { catchError, map, retry, tap } from 'rxjs/operators';
import { User } from './user';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userChanged$: Subject<User|null> = new Subject<User|null>();
  private fetchedCSRFToken = false;

  constructor(private http: HttpClient, private router: Router) {
  }

  refreshCSRFToken() {
    return this.http.get('/sanctum/csrf-cookie').subscribe();
  }

  checkLogin(): Observable<boolean> {
    return this.http.get('/api/me').pipe(
      map((res: any) => {
        if (res.success) {
          this.updateUser(res.user);
          return true;
        }
        this.updateUser(null);
        return false;
      }),
      catchError((error) => {
        if (error instanceof HttpErrorResponse && error.status === 401) {
          this.updateUser(null);
          return of(false);
        }
        return throwError(() => error);
      })
    )
  }


  login(username: string, password: string): Observable<boolean> {
    if (!this.fetchedCSRFToken) {
      this.refreshCSRFToken();
      this.fetchedCSRFToken = true;
    }
    return this.http.post('/auth/login', {username, password}).pipe(
      map((res: any) => {
        if (res.success) {
          this.updateUser(res.user);
          return true;
        }
        return false;
      })
    )
  }

  logout(): Observable<boolean> {
    // Dont bother with web request if we arnt logged in
    if (!this.isLoggedIn()) {
      return of(true);
    }

    // logout the user
    return this.http.get('/auth/logout', {}).pipe(
      map((res: any) => {
        console.log(res);
        if (res.success) {
          this.updateUser(null);
          this.router.navigateByUrl("/login");
          return true;
        }
        return false;
      }),
      catchError((error) => {
        if (error instanceof HttpErrorResponse && error.status === 401) {
          this.updateUser(null);
          this.router.navigateByUrl("/login");
          return of(true);
        }
        return throwError(() => error);
      }));
  }

  getUser(): User|null {
    let user = localStorage.getItem('user');
    if (user) {
      return JSON.parse(user);
    }
    return null;
  }

  isLoggedIn(): boolean {
    return this.getUser() !== null;
  }

  private updateUser(user: User|null) {
    this.userChanged$.next(user);
    if (user) {
      localStorage.setItem('user', JSON.stringify(user));
    } else {
      localStorage.removeItem('user');
    }
  }
}
