import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  constructor() { }

  getTheme(): string {
    let osDefault = (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) ? 'dark' : 'light';
    return localStorage.getItem('app:theme') || osDefault;
  }

  setTheme(theme: string): void {
    localStorage.setItem('app:theme', theme);
  }
}
