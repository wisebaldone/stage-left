import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Service, ServicePagedCollection} from "../../models/cmdb/service.model";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http: HttpClient) { }

  getServices(pageSize: number = 100, pageNumber: number = 1): Observable<ServicePagedCollection> {
    return this.http.get<ServicePagedCollection>(`/api/cmdb/services?page=${pageNumber}&per_page=${pageSize}`).pipe(
      map((res: ServicePagedCollection) => res)
    );
  }

  getService(id: number): Observable<Service> {
    return this.http.get(`/api/cmdb/services/${id}`).pipe(
      map((res: any) => res.data)
    );
  }
}
