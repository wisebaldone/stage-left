export class User {
  uuid: string;
  username: string;
  displayName: string;

  constructor(uuid: string, username: string, displayName: string) {
    this.uuid = uuid;
    this.username = username;
    this.displayName = displayName;
  }
}
