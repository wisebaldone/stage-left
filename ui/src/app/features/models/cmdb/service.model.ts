export interface Service {
  id: number;
  name: string;
  description: string;
  identifier: string;
  classification: string;
  tier: number;
  details: object;
  life_cycle_stage: string;
  life_cycle_status: string;
  created_at: string;
  updated_at: string;
}

export interface ServicePagedCollection {
  data: Array<Service>;
  links: {
    first: string;
    last: string;
    prev: string;
    next: string;
  };
  meta: {
    current_page: number;
    from: number;
    last_page: number;
    path: string;
    per_page: number;
    to: number;
    total: number;
    links: Array<{
      url: string;
      label: string;
      active: boolean;
    }>;
  }
}
