<?php

namespace App\Models;

use App\Models\ConfigurationItems\Service;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChangeRequest extends Model
{
    use HasFactory;
    use HasUuids;
    use SoftDeletes;

    protected $table = 'cmdb_change_requests';

    protected $fillable = [
        'name',
        'description',
        'type',
        'service_id',
        'started_at',
        'completed_at',
        'scheduled_at',
        'scheduled_end_at',
        'details',
        'life_cycle_stage',
        'life_cycle_status',
    ];

    protected $casts = [
        'details' => 'array',
    ];

    protected $dates = [
        'started_at',
        'completed_at',
        'scheduled_at',
        'scheduled_end_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

}
