<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConfigurationItemRelationship extends Model
{
    use HasFactory;
    use HasUuids;
    use SoftDeletes;

    protected $table = 'cmdb_ci_relationships';

    protected $fillable = [
        'type',
        'parent_id',
        'child_id',
    ];

    public function parent()
    {
        return $this->belongsTo(ConfigurationItem::class, 'parent_id');
    }

    public function child()
    {
        return $this->belongsTo(ConfigurationItem::class, 'child_id');
    }

}
