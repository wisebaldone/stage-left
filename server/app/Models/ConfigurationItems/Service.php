<?php

namespace App\Models\ConfigurationItems;

use App\Models\ConfigurationItem;
use App\Rules\CILifecycle;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use JeroenG\Explorer\Application\Explored;
use Laravel\Scout\Searchable;

class Service extends ConfigurationItem implements Explored
{
    use HasFactory;
    use HasUuids;
    use SoftDeletes;
    use Searchable;

    const SCHEMA_URI = 'https://stageleft/cmdb/schemas/service';

    protected $table = 'cmdb_ci_services';

    protected $attributes = [
        'uri' => 'https://stageleft/cmdb/schemas/service',
        'tier' => 4
    ];

    protected $fillable = [
        'name',
        'description',
        'identifier',
        'classification',
        'tier',
        'details',
        'life_cycle_stage',
        'life_cycle_status',
    ];

    private static array $createRules = [
        'name' => 'required|max:255',
        'description' => 'required|string',
        'identifier' => 'required|string',
        'classification' => 'required|string',
        'tier' => 'integer',
        'details' => 'required|array',
        'life_cycle_stage' => 'required|string|in:Development,Operational,Retired',
        'life_cycle_status' => 'required|string',
        'witnessed_at' => 'date',
    ];

    private static array $updateRules = [
        'name' => 'required|max:255',
        'description' => 'required|string',
        'identifier' => 'required|string',
        'classification' => 'required|string',
        'tier' => 'integer',
        'details' => 'required|array',
        'life_cycle_stage' => 'required|string|in:Development,Operational,Retired',
        'life_cycle_status' => 'required|string',
        'witnessed_at' => 'date',
    ];

    private static array $patchRules = [
        'name' => 'nullable|string|max:255',
        'description' => 'nullable|string',
        'identifier' => 'nullable|string',
        'classification' => 'nullable|string',
        'tier' => 'nullable|integer',
        'details' => 'nullable|array',
        'life_cycle_stage' => 'nullable|string',
        'life_cycle_status' => 'nullable|string',
        'witnessed_at' => 'nullable|date',
    ];

    public static function getCreateRules(): array
    {
        return array_merge(self::$createRules, [
            'life_cycle_status' => ['required', 'string', new CILifecycle]
        ]);
    }

    public static function getUpdateRules(): array
    {
        return array_merge(self::$updateRules, [
            'life_cycle_status' => ['required', 'string', new CILifecycle]
        ]);
    }

    public static function getPatchRules(): array
    {
        return array_merge(self::$patchRules, [
            'life_cycle_status' => ['nullable', 'string', new CILifecycle]
        ]);
    }

    public function toSearchableArray(): array
    {
        return $this->toArray();
    }

    public function mappableAs(): array
    {
        $base = parent::mappableAs();
        return array_merge($base, [
            'tier' => 'integer',
            'identifier' => 'keyword',
            'classification' => 'keyword',
        ]);
    }
}
