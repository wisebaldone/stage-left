<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ConfigurationItemGroupMembership extends Pivot
{
    protected $fillable = [
        'type',
        'requires_attestation',
        'attested_at'
    ];

    protected $casts = [
        'requires_attestation' => 'boolean',
        'attested_at' => 'datetime'
    ];
}
