<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConfigurationItemGroup extends Model
{
    use HasFactory;
    use HasUuids;
    use SoftDeletes;

    protected $table = 'cmdb_ci_groups';

    protected $fillable = [
        'name',
        'description',
        'query',
        'witnessed_at'
    ];

    protected $casts = [
        'witnessed_at' => 'datetime',
    ];

    public function items()
    {
        return $this->belongsToMany(ConfigurationItem::class)->using(ConfigurationItemGroupMembership::class);
    }
}
