<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Opis\JsonSchema\Errors\ErrorFormatter;
use Opis\JsonSchema\Helper;
use Opis\JsonSchema\Validator;

class ConfigurationItemSchema extends Model
{
    use HasFactory;
    use HasUuids;
    use SoftDeletes;

    protected $table = 'cmdb_ci_schemas';

    protected $fillable = [
        'uri',
        'name',
        'description',
        'type',
        'schema',
    ];

    protected $casts = [
        'schema' => 'array',
    ];

    public function runChecks($details): \Opis\JsonSchema\ValidationResult
    {
        $validator = new Validator();
        return $validator->validate(Helper::toJson($details), Helper::toJSON($this->schema));
    }
}
