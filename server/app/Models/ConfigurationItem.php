<?php

namespace App\Models;

use App\Rules\CILifecycle;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use JeroenG\Explorer\Application\Explored;
use Laravel\Scout\Searchable;

class ConfigurationItem extends Model implements Explored
{
    use HasFactory;
    use HasUuids;
    use SoftDeletes;
    use Searchable;

    protected $table = 'cmdb_cis';

    protected $fillable = [
        'name',
        'description',
        'uri',
        'details',
        'life_cycle_stage',
        'life_cycle_status',
        'witnessed_at'
    ];

    protected $attributes = [
        'uri' => 'https://stageleft/cmdb/schemas/default',
    ];

    protected $casts = [
        'details' => 'array',
        'witnessed_at' => 'datetime',
    ];

    private static array $createRules = [
        'name' => 'required|max:255',
        'description' => 'required|string',
        'uri' => 'string',
        'details' => 'required|array',
        'life_cycle_stage' => 'required|string|in:Development,Operational,Retired',
        'life_cycle_status' => 'required|string',
        'witnessed_at' => 'date',
    ];

    private static array $updateRules = [
        'name' => 'required|max:255',
        'description' => 'required|string',
        'uri' => 'required|string',
        'details' => 'required|array',
        'life_cycle_stage' => 'required|string|in:Development,Operational,Retired',
        'life_cycle_status' => 'required|string',
        'witnessed_at' => 'date',
    ];

    private static array $patchRules = [
        'name' => 'string|max:255',
        'description' => 'nullable|string',
        'uri' => 'string',
        'details' => 'nullable|array',
        'life_cycle_stage' => 'nullable|string',
        'life_cycle_status' => 'nullable|string',
        'witnessed_at' => 'nullable|date',
    ];

    public static function getCreateRules(): array
    {
        return array_merge(self::$createRules, [
            'life_cycle_status' => ['required', 'string', new CILifecycle]
        ]);
    }

    public static function getUpdateRules(): array
    {
        return array_merge(self::$updateRules, [
            'life_cycle_status' => ['required', 'string', new CILifecycle]
        ]);
    }

    public static function getPatchRules(): array
    {
        return array_merge(self::$patchRules, [
            'life_cycle_status' => ['nullable', 'string', new CILifecycle]
        ]);
    }

    public function groups(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(ConfigurationItemGroup::class)->using(ConfigurationItemGroupMembership::class);
    }

    public function schema(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(ConfigurationItemSchema::class, 'uri', 'uri');
    }

    public static function validateLifecycle($lifeCycleStage, $lifeCycleStatus): bool
    {
        $validLifeCycleStages = ['Development', 'Operational', 'Retired'];
        $validLifeCycleStatuses = [
            'Development' => ['In Design', 'In Development', 'In Testing', 'Pilot'],
            'Operational' => ['Available', 'Sunsetting', 'End Of Support'],
            'Retired' => ['Retired', 'Archived']
        ];

        if (!in_array($lifeCycleStage, $validLifeCycleStages)) {
            return false;
        }

        if (!in_array($lifeCycleStatus, $validLifeCycleStatuses[$lifeCycleStage])) {
            return false;
        }

        return true;
    }

    public function toSearchableArray(): array
    {
        $item = $this->toArray();

        // Add the relationships here
        // Add the groups

        return $item;
    }

    public function mappableAs(): array
    {
        return [
            'id' => 'keyword',
            'name' => 'text',
            'description' => 'text',
            'uri' => 'keyword',
            'details' => 'object',
            'life_cycle_stage' => 'keyword',
            'life_cycle_status' => 'keyword',
            'witnessed_at' => 'date',
            'created_at' => 'date',
            'updated_at' => 'date',
            'deleted_at' => 'date',
        ];
    }
}
