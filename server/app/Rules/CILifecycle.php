<?php

namespace App\Rules;

use App\Models\ConfigurationItem;
use Closure;
use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Translation\PotentiallyTranslatedString;

class CILifecycle implements DataAwareRule,ValidationRule
{
    protected array $data = [];

    /**
     * Run the validation rule.
     *
     * @param Closure(string): PotentiallyTranslatedString $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (!ConfigurationItem::validateLifecycle($this->data['life_cycle_stage'], $this->data['life_cycle_status'])) {
            $fail('The provided life cycle stage and status are invalid.');
        }
    }

    public function setData(array $data): static
    {
        $this->data = $data;
        return $this;
    }
}
