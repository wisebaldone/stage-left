<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('username', 'password');

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return response()->json([
                'success' => true,
                'user' => Auth::user(),
            ]);
        }

        return response()->json([
            'success' => false, 'message' => 'The provided credentials do not match our records.'
        ], 401);
    }

    public function logout(Request $request): JsonResponse
    {
        auth()->guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return response()->json([
            'success' => true,
        ]);
    }

    /**
     * SAML2 Redirect
     * @return RedirectResponse
     */
    public function samlRedirect(): RedirectResponse
    {
        return Socialite::driver('saml2')->redirect();
    }

    /**
     * SAML2 Callback
     * @param Request $request
     * @return RedirectResponse
     */
    public function samlCallback(Request $request): RedirectResponse {
        $samlUser = Socialite::driver('saml2')->user();

        $user = User::updateOrCreate([
            'username'  => $samlUser->upn
        ], [
            'username'  => $samlUser->upn,
            'name'      => $samlUser->getName(),
            'email'     => $samlUser->getEmail(),
        ]);

        Auth::login($user);

        return redirect('/');
    }


    public function status(Request $request): JsonResponse {
        return response()->json([
            'authenticated' => Auth::check(),
            'user' => Auth::user(),
        ]);
    }
}
