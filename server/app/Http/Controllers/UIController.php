<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class UIController extends Controller
{
    /**
     * Show the SPA UI.
     *
     * @return View
     */
    public function index(): View
    {
        return view('frontend');
    }
}
