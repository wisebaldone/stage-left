<?php

namespace App\Http\Controllers;

use App\Http\Resources\ConfigurationItemResource;
use App\Models\ConfigurationItem;
use App\Models\ConfigurationItemSchema;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use JeroenG\Explorer\Domain\Syntax\QueryString;
use Rs\Json\Merge\Patch;

class ConfigurationItemController extends Controller
{
    public function index(Request $request)
    {
        $pageSize = 100;
        if ($request->has('page_size') && $request->page_size <= $pageSize) {
            $pageSize = $request->page_size;
        }

        return ConfigurationItemResource::collection(ConfigurationItem::paginate($pageSize));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate(ConfigurationItem::getCreateRules());

        $uri = $validatedData['uri'] ?? 'https://stageleft/cmdb/schemas/default';
        // Dont allow duplicates against the uri name combo.
        if (ConfigurationItem::where('uri', $uri)
            ->where('name', $validatedData['name'])
            ->exists()) {
            abort(409, 'Configuration Item already exists');
        }

        // we must run against the new schema
        $schemaResult = $this->checkItemAgainstSchema((object)$validatedData['details'], $uri);
        if ($schemaResult != null) {
            return $schemaResult;
        }

        $configurationItem = ConfigurationItem::create($validatedData);
        return new ConfigurationItemResource($configurationItem, 201);
    }

    /**
     * Display the Configuration Item.
     *
     * @param Request $request
     * @param ConfigurationItem $item
     * @return ConfigurationItemResource
     */
    public function show(Request $request, ConfigurationItem $item)
    {
        return new ConfigurationItemResource($item);
    }

    public function search(Request $request)
    {
        $validatedData = $request->validate([
            'query' => 'required|string|min:1',
        ]);

        $results = ConfigurationItem::search("")
            ->within('cmdb_ci*')
            ->must(new QueryString($validatedData['query']))
            ->orderBy('updated_at', 'desc')
            ->paginate(100);

        $results->appends(['query' => $request->get('query')]);
        return ConfigurationItemResource::collection($results);
    }

    /**
     * Fully update a configuration item.
     *
     * @param Request $request
     * @param ConfigurationItem $item
     * @return ConfigurationItemResource|JsonResponse
     */
    public function update(Request $request, ConfigurationItem $item)
    {
        $validatedData = $request->validate(ConfigurationItem::getUpdateRules());

        // we must run against the new schema
        $schemaResult = $this->checkItemAgainstSchema((object)$validatedData['details'], $validatedData['uri']);
        if ($schemaResult != null) {
            return $schemaResult;
        }

        $item->update($validatedData);
        return new ConfigurationItemResource($item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ConfigurationItem  $configurationItem
     * @return ConfigurationItemResource|\Illuminate\Http\JsonResponse
     */
    public function patch(Request $request, ConfigurationItem $item)
    {
        $validatedData = $request->validate(ConfigurationItem::getPatchRules());


        $details = $item->details;
        $patchDetails = $validatedData['details'] ?? null;
        if ($patchDetails !== null) {
            $details = (new Patch())->apply((object)$details, (object)$patchDetails);
        }

        $item->fill($validatedData);
        $item->details = $details; // reapply the deeply nested details;

        $schemaResult = $this->checkItemAgainstSchema((object)$item->details, $item->uri);
        if ($schemaResult != null) {
            return $schemaResult;
        }

        $item->save();
        return new ConfigurationItemResource($item, 200);
    }

    /**
     * Soft Deletes a Configuration Item
     *
     * @param Request $request
     * @param string $id of the configuration item
     * @return JsonResponse
     */
    public function destroy(Request $request, String $id): JsonResponse
    {
        // if not found then already deleted
        $item = ConfigurationItem::find($id);
        if ($item == null) {
            return response()->json(null, 204);
        }

        $item->delete();
        return response()->json(null, 204);
    }

    private function checkItemAgainstSchema($data, $uri): ?JsonResponse
    {
        $schema = ConfigurationItemSchema::where('uri', $uri)->first();
        if ($schema == null) {
            return response()->json([
                'message' => 'The provided schema does not exist.'
            ], 400);
        }

        $schemaResult = $schema->runChecks($data);
        if (!$schemaResult->isValid()) {
            return response()->json([
                'message' => 'The provided details do not match the schema.',
                'errors' => $schemaResult->error()
            ], 400);
        }

        return null;
    }

}
