<?php

namespace App\Http\Controllers;

use App\Http\Resources\ConfigurationItemResource;
use App\Http\Resources\ServiceResource;
use App\Models\ConfigurationItem;
use App\Models\ConfigurationItems\Service;
use App\Models\ConfigurationItemSchema;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JeroenG\Explorer\Domain\Syntax\QueryString;
use Rs\Json\Merge\Patch;

class ServiceConfigurationItemController extends Controller
{

    /**
     * List all Service Configuration Items.
     *
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $pageSize = 100;
        if ($request->has('per_page') && $request->per_page <= $pageSize) {
            $pageSize = $request->per_page;
        }

        return ServiceResource::collection(Service::paginate($pageSize));
    }

    /**
     * Store a newly created Service Configuration Item in storage.
     *
     * @param Request $request
     * @return ServiceResource|JsonResponse
     */
    public function store(Request $request): ServiceResource|JsonResponse
    {
        $validatedData = $request->validate(Service::getCreateRules());

        $uri = Service::SCHEMA_URI;
        // Dont allow duplicates against the uri name combo.
        if (ConfigurationItem::where('uri', $uri)
            ->where('name', $validatedData['name'])
            ->exists()) {
            abort(409, 'Service already exists');
        }

        // we must run against the new schema
        $schemaResult = $this->checkItemAgainstSchema((object)$validatedData['details'], $uri);
        if ($schemaResult != null) {
            return $schemaResult;
        }

        $service = Service::create($validatedData);
        return new ServiceResource($service);
    }

    /**
     * Display the Configuration Item.
     *
     * @param Request $request
     * @param Service $service
     * @return ServiceResource
     */
    public function show(Request $request, Service $service): ServiceResource
    {
        return new ServiceResource($service);
    }

    public function search(Request $request)
    {
        $validatedData = $request->validate([
            'query' => 'required|string|min:1',
        ]);

        $results = Service::search("")
            ->must(new QueryString($validatedData['query']))
            ->orderBy('updated_at', 'desc')
            ->paginate(100);

        $results->appends(['query' => $request->get('query')]);
        return ServiceResource::collection($results);
    }

    /**
     * Fully update a configuration item.
     *
     * @param Request $request
     * @param Service $service
     * @return ServiceResource|JsonResponse
     */
    public function update(Request $request, Service $service): JsonResponse|ServiceResource
    {
        $validatedData = $request->validate(Service::getUpdateRules());

        // Schema is always service
        $schemaResult = $this->checkItemAgainstSchema((object)$validatedData['details'], Service::SCHEMA_URI);
        if ($schemaResult != null) {
            return $schemaResult;
        }

        $service->update($validatedData);
        return new ServiceResource($service);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Service $service
     * @return ServiceResource|JsonResponse
     */
    public function patch(Request $request, Service $service): JsonResponse|ServiceResource
    {
        $validatedData = $request->validate(Service::getPatchRules());

        $details = $service->details;
        $patchDetails = $validatedData['details'] ?? null;
        if ($patchDetails !== null) {
            $details = (new Patch())->apply((object)$details, (object)$patchDetails);
        }

        $service->fill($validatedData);
        $service->details = $details; // reapply the deeply nested details;

        $schemaResult = $this->checkItemAgainstSchema((object)$service->details, Service::SCHEMA_URI);
        if ($schemaResult != null) {
            return $schemaResult;
        }

        $service->save();
        return new ServiceResource($service);
    }

    /**
     * Soft Deletes a Configuration Item
     *
     * @param Request $request
     * @param string $id of the configuration item
     * @return JsonResponse
     */
    public function destroy(Request $request, string $id): JsonResponse
    {
        // if not found then already deleted
        $item = ConfigurationItem::find($id);
        if ($item == null) {
            return response()->json(null, 204);
        }

        $item->delete();
        return response()->json(null, 204);
    }

    private function checkItemAgainstSchema($data, $uri): ?JsonResponse
    {
        /* @var ConfigurationItemSchema $schema */
        $schema = ConfigurationItemSchema::where('uri', $uri)->first();
        if ($schema == null) {
            return response()->json([
                'message' => 'The provided schema does not exist.'
            ], 400);
        }

        $schemaResult = $schema->runChecks($data);
        if (!$schemaResult->isValid()) {
            return response()->json([
                'message' => 'The provided details do not match the schema.',
                'errors' => $schemaResult->error()
            ], 400);
        }

        return null;
    }
}
