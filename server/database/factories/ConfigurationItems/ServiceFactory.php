<?php

namespace Database\Factories\ConfigurationItems;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Service>
 */
class ServiceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return $this->faker->randomElement([
            [$this, 'generateDefault']
        ])();
    }

    /**
     * generateLifeCycle - Generate a life cycle stage and status.
     *
     * @return array<string, mixed>
     */
    private function generateLifeCycle()
    {
        $lifeCycleStage = $this->faker->randomElement(['Development', 'Operational', 'Retired']);

        $lifeCycleStatus = 'Invalid';
        switch ($lifeCycleStage) {
            case 'Development':
                $lifeCycleStatus = $this->faker->randomElement(['In Design', 'In Development', 'In Testing', 'Pilot']);
                break;
            case 'Operational':
                $lifeCycleStatus = $this->faker->randomElement(['Available', 'Sunsetting', 'End Of Support']);
                break;
            case 'Retired':
                $lifeCycleStatus = $this->faker->randomElement(['Retired', 'Archived']);
                break;
        }

        return [
            'life_cycle_stage' => $lifeCycleStage,
            'life_cycle_status' => $lifeCycleStatus
        ];
    }

    private function generateDefault()
    {
        return [
                'name' => $this->faker->name,
                'description' => $this->faker->sentence,
                'identifier' => $this->faker->slug,
                'classification' => $this->faker->randomElement(['Technical', 'Business', 'Application']),
                'tier' => $this->faker->numberBetween(1, 4),
                'uri' => 'https://stageleft/cmdb/schemas/service',
                'details' => [
                    'tags' => $this->faker->words(2),
                ],
            ] + $this->generateLifeCycle();
    }
}
