<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\ConfigurationItemRelationship;
use Illuminate\Database\Seeder;
use function Illuminate\Events\queueable;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            CampaignSeeder::class,
            ChangeRequestSeeder::class,
            ConfigurationItemGroupSeeder::class,
            ConfigurationItemRelationshipSeeder::class,
            ConfigurationItemSchemaSeeder::class,
            ConfigurationItemSeeder::class,
            GroupSeeder::class,
            IncidentSeeder::class,
            ServiceConfigurationItemSeeder::class
        ]);
    }
}
