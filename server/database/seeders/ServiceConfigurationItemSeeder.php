<?php

namespace Database\Seeders;

use App\Models\ConfigurationItems\Service;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ServiceConfigurationItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Service::factory(1000)->create();
    }
}
