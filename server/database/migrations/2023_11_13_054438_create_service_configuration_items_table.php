<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cmdb_ci_services', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->text('name');
            $table->text('description')->nullable();
            $table->text('uri');
            $table->text('identifier')->nullable();
            $table->text('classification')->nullable();
            $table->integer('tier')->nullable();
            $table->json('details')->nullable();
            $table->text('life_cycle_stage')->nullable();
            $table->text('life_cycle_status')->nullable();
            $table->timestamp('witnessed_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement('ALTER TABLE cmdb_ci_services inherit cmdb_cis');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cmdb_ci_services');
    }
};
