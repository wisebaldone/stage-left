<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cmdb_cis', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->text('name');
            $table->text('description')->nullable();
            $table->text('uri');
            $table->json('details')->nullable();
            $table->text('life_cycle_stage')->nullable();
            $table->text('life_cycle_status')->nullable();
            $table->timestamp('witnessed_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['name', 'uri']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        schema::table('cmdb_cis', function(Blueprint $table) {
            $table->dropUnique(['name', 'uri']);
        });
        Schema::dropIfExists('cmdb_cis');
    }
};
