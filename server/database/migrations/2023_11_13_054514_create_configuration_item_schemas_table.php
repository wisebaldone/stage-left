<?php

use App\Models\ConfigurationItemSchema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cmdb_ci_schemas', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('uri')->unique();
            $table->text('name');
            $table->text('description')->nullable();
            $table->json('schema');
            $table->timestamps();
            $table->softDeletes();
        });

        $default = new ConfigurationItemSchema();
        $default->id = '00000000-0000-0000-0000-000000000001';
        $default->uri = 'https://stageleft/cmdb/schemas/default';
        $default->name = 'Default';
        $default->description = 'Default schema for configuration items.';
        $default->schema = json_decode(<<<'JSON'
{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https://stageleft/cmdb/schemas/default",
    "title": "Default",
    "description": "A Configuration Item",
    "type": "object",
    "properties": {
        "labels": {
            "description": "Labels are optional key/value pairs of the item which reference internal items, restricted to strings-strings",
            "type": "object",
            "additionalProperties": {
                "type": "string"
            }
        },
        "annotations": {
            "description": "Annotations are optional key/value pairs which reference external items, restricted to strings-any",
            "type": "object",
            "additionalProperties": true
        },
        "tags": {
            "description": "A list of strings",
            "type": "array",
            "uniqueItems": true,
            "items": {
                "type": "string"
            }
        },
        "links": {
            "description": "List of links for the item",
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "url": {
                        "description": "Url for the link",
                        "type": "string"
                    },
                    "title": {
                        "description": "Name of the link",
                        "type": "string"
                    },
                    "icon": {
                        "type": "string"
                    },
                    "type": {
                        "type": "string"
                    }
                }
            }
        }
    },
    "additionalProperties": false
}
JSON);


        $default->save();

        $service = new ConfigurationItemSchema();
        $service->id = '00000000-0000-0000-0000-000000000002';
        $service->uri = 'https://stageleft/cmdb/schemas/service';
        $service->name = 'Service';
        $service->description = 'Default schema for configuration items.';
        $service->schema = json_decode(<<<'JSON'
{
    "$schema": "https://json-schema.org/draft-07/schema#",
    "$id": "https://stageleft/cmdb/schemas/service",
    "title": "Service",
    "description": "A Service (Business, Technical or Application)",
    "type": "object",
    "properties": {
        "labels": {
            "description": "Labels are optional key/value pairs of the item which reference internal items, restricted to strings-strings",
            "type": "object",
            "additionalProperties": {
                "type": "string"
            }
        },
        "annotations": {
            "description": "Annotations are optional key/value pairs which reference external items, restricted to strings-any",
            "type": "object",
            "additionalProperties": true
        },
        "tags": {
            "description": "A list of strings",
            "type": "array",
            "uniqueItems": true,
            "items": {
                "type": "string"
            }
        },
        "links": {
            "description": "List of links for the item",
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "url": {
                        "description": "Url for the link",
                        "type": "string"
                    },
                    "title": {
                        "description": "Name of the link",
                        "type": "string"
                    },
                    "icon": {
                        "type": "string"
                    },
                    "type": {
                        "type": "string"
                    }
                }
            }
        }
    },
    "additionalProperties": false
}
JSON);

        $service->save();
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cmdb_ci_schemas');
    }
};
