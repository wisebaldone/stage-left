<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cmdb_ci_relationships', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->text('type');
            $table->uuid('parent_id');
            $table->uuid('child_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('parent_id')->references('id')->on('cmdb_cis');
            $table->foreign('child_id')->references('id')->on('cmdb_cis');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('cmdb_ci_relationships', function(Blueprint $table) {
            $table->dropForeign(['parent_id']);
            $table->dropForeign(['child_id']);
        });
        Schema::dropIfExists('cmdb_ci_relationships');
    }
};
