<?php

use App\Http\Controllers\UIController;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'auth'], function() {
    Route::get('login', function() {
        return redirect('/'); // SPA handles login
    })->name('login');
    Route::post('login', [AuthController::class, 'login']);
    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::post('logout', [AuthController::class, 'logout']);
    });
    Route::get('saml/redirect', [AuthController::class, 'samlRedirect']);
    Route::get('saml/callback', [AuthController::class, 'samlCallback']);
});

Route::any('/{any}', [UIController::class, 'index'])->where('any', '^((?!api|auth).)*$');
