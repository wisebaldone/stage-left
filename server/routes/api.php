<?php

use App\Http\Controllers\CampaignController;
use App\Http\Controllers\ChangeRequestController;
use App\Http\Controllers\ConfigurationItemController;
use App\Http\Controllers\ConfigurationItemGroupController;
use App\Http\Controllers\ConfigurationItemSchemaController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\IncidentController;
use App\Http\Controllers\ServiceConfigurationItemController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([], function() {
    Route::group(['prefix' => 'users'], function () {
        Route::get('/', [UserController::class, 'index']);
        Route::post('/search', [UserController::class, 'search']);
        Route::get('/{id}', [UserController::class, 'show']);
        Route::post('/', [UserController::class, 'store']);
        Route::put('/{id}', [UserController::class, 'update']);
        Route::delete('/{id}', [UserController::class, 'destroy']);
    });

    Route::group(['prefix' => 'groups'], function () {
        Route::get('/', [GroupController::class, 'index']);
        Route::post('/search', [GroupController::class, 'search']);
        Route::get('/{id}', [GroupController::class, 'show']);
        Route::post('/', [GroupController::class, 'store']);
        Route::put('/{id}', [GroupController::class, 'update']);
        Route::delete('/{id}', [GroupController::class, 'destroy']);
    });

    Route::group(['prefix' => 'campaigns'], function () {
        Route::get('/', [CampaignController::class, 'index']);
        Route::post('/search', [CampaignController::class, 'search']);
        Route::get('/{id}', [CampaignController::class, 'show']);
        Route::post('/', [CampaignController::class, 'store']);
        Route::put('/{id}', [CampaignController::class, 'update']);
        Route::delete('/{id}', [CampaignController::class, 'destroy']);
    });

    Route::group(['prefix' => 'cmdb'], function () {

        Route::group(['prefix' => 'items'], function () {
            Route::get('/', [ConfigurationItemController::class, 'index']);
            Route::get('/search', [ConfigurationItemController::class, 'search']);
            Route::get('/{item}', [ConfigurationItemController::class, 'show']);
            Route::post('/', [ConfigurationItemController::class, 'store']);
            Route::put('/{item}', [ConfigurationItemController::class, 'update']);
            Route::patch('/{item}', [ConfigurationItemController::class, 'patch']);
            Route::delete('/{id}', [ConfigurationItemController::class, 'destroy']);
            Route::group(['prefix' => '/{item}/relations'], function () {
                Route::get('/', [ConfigurationItemController::class, 'relations']);
                Route::post('/', [ConfigurationItemController::class, 'attachRelations']);
                Route::delete('/{id}', [ConfigurationItemController::class, 'detachRelation']);
            });
        });

        Route::group(['prefix' => 'services'], function () {
            Route::get('/', [ServiceConfigurationItemController::class, 'index']);
            Route::post('/search', [ServiceConfigurationItemController::class, 'search']);
            Route::get('/{service}', [ServiceConfigurationItemController::class, 'show']);
            Route::post('/', [ServiceConfigurationItemController::class, 'store']);
            Route::put('/{service}', [ServiceConfigurationItemController::class, 'update']);
            Route::patch('/{service}', [ServiceConfigurationItemController::class, 'patch']);
            Route::delete('/{id}', [ServiceConfigurationItemController::class, 'destroy']);
        });

        Route::group(['prefix' => 'requests'], function () {
            Route::get('/', [ChangeRequestController::class, 'index']);
            Route::post('/search', [ChangeRequestController::class, 'search']);
            Route::get('/{id}', [ChangeRequestController::class, 'show']);
            Route::post('/', [ChangeRequestController::class, 'store']);
            Route::put('/{id}', [ChangeRequestController::class, 'update']);
            Route::patch('/{id}', [ChangeRequestController::class, 'patch']);
            Route::delete('/{id}', [ChangeRequestController::class, 'destroy']);
        });

        Route::group(['prefix' => 'incidents'], function () {
            Route::get('/', [IncidentController::class, 'index']);
            Route::post('/search', [IncidentController::class, 'search']);
            Route::get('/{id}', [IncidentController::class, 'show']);
            Route::post('/', [IncidentController::class, 'store']);
            Route::put('/{id}', [IncidentController::class, 'update']);
            Route::patch('/{id}', [IncidentController::class, 'patch']);
            Route::delete('/{id}', [IncidentController::class, 'destroy']);
        });

        Route::group(['prefix' => 'schemas'], function () {
            Route::get('/', [ConfigurationItemSchemaController::class, 'index']);
            Route::get('/{id}', [ConfigurationItemSchemaController::class, 'show']);
        });

        Route::group(['prefix' => 'groups'], function () {
            Route::get('/', [ConfigurationItemGroupController::class, 'index']);
            Route::post('/search', [ConfigurationItemGroupController::class, 'search']);
            Route::get('/{id}', [ConfigurationItemGroupController::class, 'show']);
            Route::post('/', [ConfigurationItemGroupController::class, 'store']);
            Route::put('/{id}', [ConfigurationItemGroupController::class, 'update']);
            Route::delete('/{id}', [ConfigurationItemGroupController::class, 'destroy']);
            Route::group(['prefix' => '{group_id}/items'], function () {
                Route::get('/', [ConfigurationItemGroupController::class, 'items']);
                Route::post('/', [ConfigurationItemGroupController::class, 'attachItems']);
                Route::delete('/{id}', [ConfigurationItemGroupController::class, 'detachItem']);
            });
        });
    });
});
