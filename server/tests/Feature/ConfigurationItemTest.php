<?php

namespace Tests\Feature;

use App\Models\ConfigurationItem;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Assert as PHPUnit;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ConfigurationItemTest extends TestCase
{
    use RefreshDatabase;

    public function testCreatingCI() {
        Sanctum::actingAs(
            \App\Models\User::factory()->create(),
            ['*']
        );

        $this->seed();

        $response = $this->post('/api/cmdb/items', [
            'name' => 'test',
            'description' => 'testing to see if one of the schemas will validate',
            'details' => [
                'tags' => ['test', 'testing', 'testy']
            ],
            'life_cycle_stage' => 'Operational',
            'life_cycle_status' => 'Available',
            'witnessed_at' => '2021-01-01 00:00:00'
        ], [
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(201);
        $response->assertJson([
            'data' => [
                'name' => 'test',
                'description' => 'testing to see if one of the schemas will validate',
                'uri' => 'https://stageleft/cmdb/schemas/default',
                'details' => [
                    'tags' => ['test', 'testing', 'testy']
                ],
                'life_cycle_stage' => 'Operational',
                'life_cycle_status' => 'Available',
                'witnessed_at' => '2021-01-01T00:00:00.000000Z'
            ]
        ]);
    }
    public function testCreatingFullCI() {
        Sanctum::actingAs(
            \App\Models\User::factory()->create(),
            ['*']
        );

        $this->seed();

        $response = $this->post('/api/cmdb/items', [
            'name' => 'test',
            'description' => 'testing to see if one of the schemas will validate',
            'details' => [
                'labels' => ['hello' => 'world'],
                'tags' => ['test', 'testing', 'testy']
            ],
            'life_cycle_stage' => 'Operational',
            'life_cycle_status' => 'Available',
            'witnessed_at' => '2021-01-01 00:00:00'
        ], [
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(201);
        $response->assertJson([
            'data' => [
                'name' => 'test',
                'description' => 'testing to see if one of the schemas will validate',
                'uri' => 'https://stageleft/cmdb/schemas/default',
                'details' => [
                    'tags' => ['test', 'testing', 'testy'],
                    'labels' => ['hello' => 'world'],
                ],
                'life_cycle_stage' => 'Operational',
                'life_cycle_status' => 'Available',
                'witnessed_at' => '2021-01-01T00:00:00.000000Z'
            ]
        ]);
    }


    public function testCreatingWithInvalidSchema() {
        Sanctum::actingAs(
            \App\Models\User::factory()->create(),
            ['*']
        );

        $response = $this->post('/api/cmdb/items', [
            'name' => 'test',
            'description' => 'testing to see if one of the schemas will validate',
            'uri' => 'https://stageleft/cmdb/schemas/invalid',
            'details' => [
                'tags' => ['test', 'testing', 'testy']
            ],
            'life_cycle_stage' => 'Operational',
            'life_cycle_status' => 'Available',
            'witnessed_at' => '2021-01-01 00:00:00'
        ]);

        $response->assertStatus(400);
        $response->assertJson([
            'message' => 'The provided schema does not exist.'
        ]);
    }

    public function testListingCIs() {
        Sanctum::actingAs(
            \App\Models\User::factory()->create(),
            ['*']
        );

        $this->seed();

        $response = $this->get('/api/cmdb/items');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'description',
                    'uri',
                    'details',
                    'life_cycle_stage',
                    'life_cycle_status',
                    'witnessed_at',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ]
            ],
            'links' => [
                'first',
                'last',
                'prev',
                'next',
            ],
            'meta' => [
                'current_page',
                'from',
                'last_page',
                'links',
                'path',
                'per_page',
                'to',
                'total',
            ],
        ]);
        $response->assertJsonCount(20, 'data');
    }

    public function testListingCIsWithMaxCount() {
        Sanctum::actingAs(
            \App\Models\User::factory()->create(),
            ['*']
        );

        $this->seed();

        $response = $this->get('/api/cmdb/items?page_size=24');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'description',
                    'uri',
                    'details',
                    'life_cycle_stage',
                    'life_cycle_status',
                    'witnessed_at',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ]
            ],
            'links' => [
                'first',
                'last',
                'prev',
                'next',
            ],
            'meta' => [
                'current_page',
                'from',
                'last_page',
                'links',
                'path',
                'per_page',
                'to',
                'total',
            ],
        ]);
        $response->assertJson(['meta'=>['per_page'=>24]], 'Default page size should be 24');
    }

    public function testGettingACI() {
        Sanctum::actingAs(
            \App\Models\User::factory()->create(),
            ['*']
        );

        $this->seed();

        // get the first entry with a UUID
        $response = $this->get('/api/cmdb/items');

        $response->assertStatus(200);
        $uuid = $response->json('data.0.id');

        $response = $this->get('/api/cmdb/items/' . $uuid);
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'id' => $uuid,
            ]
        ]);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'description',
                'uri',
                'details',
                'life_cycle_stage',
                'life_cycle_status',
                'witnessed_at',
                'created_at',
                'updated_at',
                'deleted_at',
            ]
        ]);
    }

    public function testSoftDeletingCI() {
        Sanctum::actingAs(
            \App\Models\User::factory()->create(),
            ['*']
        );

        $this->seed();

        $response = $this->post('/api/cmdb/items', [
            'name' => 'test',
            'description' => 'testing to see if one of the schemas will validate',
            'uri' => 'https://stageleft/cmdb/schemas/default',
            'details' => [
                'tags' => ['test', 'testing', 'testy']
            ],
            'life_cycle_stage' => 'Operational',
            'life_cycle_status' => 'Available',
        ]);

        $response->assertStatus(201);

        $uuid = $response->json('data.id');

        $response = $this->delete('/api/cmdb/items/' . $uuid);

        $response->assertStatus(204);

        $response = $this->get('/api/cmdb/items/' . $uuid);

        $response->assertStatus(404);
    }

    public function testUpdateCI() {
        Sanctum::actingAs(
            \App\Models\User::factory()->create(),
            ['*']
        );

        $this->seed();

        $response = $this->post('/api/cmdb/items', [
            'name' => 'test',
            'description' => 'testing to see if one of the schemas will validate',
            'uri' => 'https://stageleft/cmdb/schemas/default',
            'details' => [
                'tags' => ['test', 'testing', 'testy']
            ],
            'life_cycle_stage' => 'Operational',
            'life_cycle_status' => 'Available',
        ]);

        $response->assertStatus(201);

        $response = $this->put('/api/cmdb/items/' . $response->json('data.id'), [
            'id' => $response->json('data.id'),
            'name' => 'test',
            'description' => 'testing to see if one of the schemas will validate',
            'uri' => 'https://stageleft/cmdb/schemas/default',
            'details' => [
                'tags' => ['edited']
            ],
            'life_cycle_stage' => 'Operational',
            'life_cycle_status' => 'Available',
        ]);

        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'details' => [
                    'tags' => ['edited']
                ]
            ]
        ]);
    }

    public function testPartialUpdate() {
        Sanctum::actingAs(
            \App\Models\User::factory()->create(),
            ['*']
        );

        $this->seed();

        $response = $this->post('/api/cmdb/items', [
            'name' => 'test',
            'description' => 'testing to see if one of the schemas will validate',
            'uri' => 'https://stageleft/cmdb/schemas/default',
            'details' => [
                'tags' => ['test', 'testing', 'testy']
            ],
            'life_cycle_stage' => 'Operational',
            'life_cycle_status' => 'Available',
        ]);

        $response->assertStatus(201);
        $uuid = $response->json('data.id');

        $response->assertJson([
            'data' => [
                'details' => [
                    'tags' => ['test', 'testing', 'testy']
                ]
            ]
        ]);

        $response = $this->patch('/api/cmdb/items/' . $uuid, [
            'description' => 'Updating the CIs description',
        ], [
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'description' => 'Updating the CIs description',
            ]
        ]);

        $response = $this->patch('/api/cmdb/items/' . $uuid, [
            'details' => [
                'tags' => ['test', 'testing', 'testy']
            ],
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'details' => [
                    'tags' => ['test', 'testing', 'testy']
                ],
            ]
        ]);

        $response = $this->patch('/api/cmdb/items/' . $uuid, [
            'details' => [
                'labels' => [
                    'hello' => 'world'
                ]
            ]
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'details' => [
                    'tags' => ['test', 'testing', 'testy'],
                    'labels' => [
                        'hello' => 'world'
                    ]
                ]
            ]
        ]);

        $response = $this->patch('/api/cmdb/items/' . $uuid, [
            'details' => [
                'labels' => null
            ],
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'details' => [
                    'tags' => ['test', 'testing', 'testy'],
                ]
            ]
        ]);
    }

    public function testDuplicateCIs() {
        Sanctum::actingAs(
            \App\Models\User::factory()->create(),
            ['*']
        );

        $this->seed();

        $response = $this->post('/api/cmdb/items', [
            'name' => 'test',
            'description' => 'testing to see if one of the schemas will validate',
            'uri' => 'https://stageleft/cmdb/schemas/default',
            'details' => [
                'tags' => ['test', 'testing', 'testy'],
            ],
            'life_cycle_stage' => 'Operational',
            'life_cycle_status' => 'Available',
        ]);

        $response->assertStatus(201);

        $response = $this->post('/api/cmdb/items', [
            'name' => 'test',
            'description' => 'testing to see if one of the schemas will validate',
            'uri' => 'https://stageleft/cmdb/schemas/default',
            'details' => [
                'tags' => ['test', 'testing', 'testy'],
            ],
            'life_cycle_stage' => 'Operational',
            'life_cycle_status' => 'Available',
        ]);

        $response->assertStatus(409);
    }

//    public function testItemToSearchableArray() {
//        Sanctum::actingAs(
//            \App\Models\User::factory()->create(),
//            ['*']
//        );
//
//        $this->seed();
//
//        $info = ConfigurationItem::first()->toSearchableArray();
//        print $info;
//    }

// test being able to search for an item
    public function testSearchingCIs() {
        Sanctum::actingAs(
            \App\Models\User::factory()->create(),
            ['*']
        );

        $this->seed();

        $response = $this->post('/api/cmdb/items', [
            'name' => 'BatCar',
            'description' => 'The Bat Car',
            'uri' => 'https://stageleft/cmdb/schemas/default',
            'details' => [
                'tags' => ['test', 'batman'],
            ],
            'life_cycle_stage' => 'Operational',
            'life_cycle_status' => 'Available',
        ]);

        $response->assertStatus(201);

        $response = $this->get('/api/cmdb/items/search?query=name:BatCar');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'description',
                    'uri',
                    'details',
                    'life_cycle_stage',
                    'life_cycle_status',
                    'witnessed_at',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ]
            ],
            'links' => [
                'first',
                'last',
                'prev',
                'next',
            ],
            'meta' => [
                'current_page',
                'from',
                'last_page',
                'links',
                'path',
                'per_page',
                'to',
                'total',
            ],
        ]);
    }

}
