<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Assert as PHPUnit;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ServiceConfigurationItemTest extends TestCase
{
    use RefreshDatabase;

    public function testCreatingService() {
        Sanctum::actingAs(
            \App\Models\User::factory()->create(),
            ['*']
        );

        $this->seed();

        $response = $this->post('/api/cmdb/services', [
            'name' => 'Test Service',
            'description' => 'A Test Service',
            'identifier' => 'stageleft/example',
            'classification' => 'Technical',
            'details' => [
                'links' => [
                    ['url' => 'https://example.com', 'description' => 'Example', 'type' => 'homepage'],
                ]
            ],
            'life_cycle_stage' => 'Operational',
            'life_cycle_status' => 'Available'
        ]);

        $response->assertStatus(201);
        $response->assertJson([
            'data' => [
                'name' => 'Test Service',
                'description' => 'A Test Service',
                'identifier' => 'stageleft/example',
                'classification' => 'Technical',
                'details' => [
                    'links' => [
                        ['url' => 'https://example.com', 'description' => 'Example', 'type' => 'homepage'],
                    ]
                ],
                'life_cycle_stage' => 'Operational',
                'life_cycle_status' => 'Available'
            ]
        ]);
    }

    public function testCreatingWithSchemaOverride() {
        Sanctum::actingAs(
            \App\Models\User::factory()->create(),
            ['*']
        );

        $response = $this->post('/api/cmdb/services', [
            'name' => 'Test Service',
            'description' => 'A Test Service',
            'identifier' => 'stageleft/example',
            'classification' => 'Technical',
            'details' => [
                'links' => [
                    ['url' => 'https://example.com', 'description' => 'Example', 'type' => 'homepage'],
                ]
            ],
            'life_cycle_stage' => 'Operational',
            'life_cycle_status' => 'Available'
        ]);

        $response->assertStatus(201);
        $response->assertJson([
            'data' => [
                'details' => [
                    'links' => [
                        ['url' => 'https://example.com', 'description' => 'Example', 'type' => 'homepage'],
                    ]
                ]
            ]
        ]);
    }

    public function testListingServices() {
        Sanctum::actingAs(
            \App\Models\User::factory()->create(),
            ['*']
        );

        $this->seed();

        $response = $this->get('/api/cmdb/services');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'description',
                    'uri',
                    'identifier',
                    'classification',
                    'tier',
                    'details',
                    'life_cycle_stage',
                    'life_cycle_status',
                    'witnessed_at',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ]
            ],
            'links' => [
                'first',
                'last',
                'prev',
                'next',
            ],
            'meta' => [
                'current_page',
                'from',
                'last_page',
                'links',
                'path',
                'per_page',
                'to',
                'total',
            ],
        ]);
        $response->assertJsonCount(10, 'data');
    }

    public function testListingServicesWithMaxCount() {
        Sanctum::actingAs(
            \App\Models\User::factory()->create(),
            ['*']
        );

        $this->seed();

        $response = $this->get('/api/cmdb/services?page_size=24');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'description',
                    'uri',
                    'identifier',
                    'classification',
                    'tier',
                    'details',
                    'life_cycle_stage',
                    'life_cycle_status',
                    'witnessed_at',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ]
            ],
            'links' => [
                'first',
                'last',
                'prev',
                'next',
            ],
            'meta' => [
                'current_page',
                'from',
                'last_page',
                'links',
                'path',
                'per_page',
                'to',
                'total',
            ],
        ]);
        $response->assertJson(['meta'=>['per_page'=>24]], 'Default page size should be 24');
    }

    public function testGettingAService() {
        Sanctum::actingAs(
            \App\Models\User::factory()->create(),
            ['*']
        );

        $this->seed();

        // get the first entry with a UUID
        $response = $this->get('/api/cmdb/services');

        $response->assertStatus(200);
        $uuid = $response->json('data.0.id');

        $response = $this->get('/api/cmdb/services/' . $uuid);
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'id' => $uuid,
            ]
        ]);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'description',
                'uri',
                'details',
                'life_cycle_stage',
                'life_cycle_status',
                'witnessed_at',
                'created_at',
                'updated_at',
                'deleted_at',
            ]
        ]);
    }

    public function testSoftDeletingCI() {
        Sanctum::actingAs(
            \App\Models\User::factory()->create(),
            ['*']
        );

        $this->seed();

        $response = $this->post('/api/cmdb/services', [
            'name' => 'Test Service',
            'description' => 'A Test Service',
            'identifier' => 'stageleft/example',
            'classification' => 'Technical',
            'details' => [
                'links' => [
                    ['url' => 'https://example.com', 'description' => 'Example', 'type' => 'homepage'],
                ]
            ],
            'life_cycle_stage' => 'Operational',
            'life_cycle_status' => 'Available'
        ]);

        $response->assertStatus(201);

        $uuid = $response->json('data.id');

        $response = $this->delete('/api/cmdb/services/' . $uuid);

        $response->assertStatus(204);

        $response = $this->get('/api/cmdb/services/' . $uuid);

        $response->assertStatus(404);
    }

    public function testUpdateService() {
        Sanctum::actingAs(
            \App\Models\User::factory()->create(),
            ['*']
        );

        $this->seed();

        $response = $this->post('/api/cmdb/services', [
            'name' => 'Test Service',
            'description' => 'A Test Service',
            'identifier' => 'stageleft/example',
            'classification' => 'Technical',
            'details' => [
                'links' => [
                    ['url' => 'https://example.com', 'description' => 'Example', 'type' => 'homepage'],
                ]
            ],
            'life_cycle_stage' => 'Operational',
            'life_cycle_status' => 'Available'
        ]);

        $response->assertStatus(201);

        $response = $this->put('/api/cmdb/services/' . $response->json('data.id'), [
            'id' => $response->json('data.id'),
            'name' => 'Test Service',
            'description' => 'A Test Service',
            'identifier' => 'stageleft/example',
            'classification' => 'Technical',
            'details' => [
                'links' => [
                    ['url' => 'https://example.com', 'description' => 'Example 2', 'type' => 'homepage'],
                ]
            ],
            'life_cycle_stage' => 'Operational',
            'life_cycle_status' => 'Available'
        ]);

        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'details' => [
                    'links' => [
                        ['url' => 'https://example.com', 'description' => 'Example 2', 'type' => 'homepage'],
                    ]
                ]
            ]
        ]);
    }

    public function testPartialUpdate() {
        Sanctum::actingAs(
            \App\Models\User::factory()->create(),
            ['*']
        );

        $this->seed();

        $response = $this->post('/api/cmdb/services', [
            'name' => 'Test Service',
            'description' => 'A Test Service',
            'identifier' => 'stageleft/example',
            'classification' => 'Technical',
            'details' => [
                'links' => [
                    ['url' => 'https://example.com', 'description' => 'Example', 'type' => 'homepage'],
                ]
            ],
            'life_cycle_stage' => 'Operational',
            'life_cycle_status' => 'Available'
        ]);

        $response->assertStatus(201);
        $uuid = $response->json('data.id');

        $response->assertJson([
            'data' => [
                'details' => [
                    'links' => [
                        ['url' => 'https://example.com', 'description' => 'Example', 'type' => 'homepage'],
                    ]
                ]
            ]
        ]);

        $response = $this->patch('/api/cmdb/services/' . $uuid, [
            'description' => 'Updating the CIs description',
        ], [
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'description' => 'Updating the CIs description',
            ]
        ]);

        $response = $this->patch('/api/cmdb/services/' . $uuid, [
            'details' => [
                'tags' => ['have edited the details']
            ],
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'details' => [
                    'tags' => ['have edited the details']
                ]
            ]
        ]);

        $response = $this->patch('/api/cmdb/services/' . $uuid, [
            'details' => [
                'tags' => null
            ],
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'details' => [
                    'links' => [
                        ['url' => 'https://example.com', 'description' => 'Example', 'type' => 'homepage'],
                    ]
                ]
            ]
        ]);
    }

    public function testDuplicateCIs() {
        Sanctum::actingAs(
            \App\Models\User::factory()->create(),
            ['*']
        );

        $this->seed();

        $response = $this->post('/api/cmdb/services', [
            'name' => 'Test Service',
            'description' => 'A Test Service',
            'identifier' => 'stageleft/example',
            'classification' => 'Technical',
            'details' => [
                'links' => [
                    ['url' => 'https://example.com', 'description' => 'Example', 'type' => 'homepage'],
                ]
            ],
            'life_cycle_stage' => 'Operational',
            'life_cycle_status' => 'Available'
        ]);

        $response->assertStatus(201);

        $response = $this->post('/api/cmdb/services', [
            'name' => 'Test Service',
            'description' => 'A Test Service',
            'identifier' => 'stageleft/example',
            'classification' => 'Technical',
            'details' => [
                'links' => [
                    ['url' => 'https://example.com', 'description' => 'Example', 'type' => 'homepage'],
                ]
            ],
            'life_cycle_stage' => 'Operational',
            'life_cycle_status' => 'Available'
        ]);

        $response->assertStatus(409);
    }

}
